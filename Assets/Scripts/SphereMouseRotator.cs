﻿using UnityEngine;
using System.Collections;

public class SphereMouseRotator : MonoBehaviour {

		public bool isRotating;
		public bool toLeft;
		public bool hasTest;
		public Test myTest;
		[Range(0,100)]
		public float radius;
		[Range(-100, 100)]
		public float height;
		[Range(0,100)]
		public float speed;
	public GameObject pivot;

		// Use this for initialization
		void Start () {
				Vector3 distep = transform.position.normalized;
				transform.position = distep * radius;
				transform.forward = -distep;
				if (GetComponent<Test> () != null) {
						myTest = GetComponent<Test> ();
						myTest.enabled = !isRotating;
				}
						
		}
		
		// Update is called once per frame
		void Update () {
				if (Input.GetKeyDown (KeyCode.Tab)) {
						isRotating = !isRotating;
						if(myTest != null)
								myTest.enabled = !isRotating;
				}
				if (Input.GetKeyDown (KeyCode.CapsLock)) {
						toLeft = !toLeft;
				}
				if (isRotating) 
				{		
						int isLeft = toLeft ? -1 : 1;
						transform.position += transform.right*speed*isLeft;
					
//						Vector3 dist = new Vector3(transform.position.x, height, transform.position.z);
						Vector3 dist = transform.position - pivot.transform.position;
						Vector3 NewPos = pivot.transform.position + dist.normalized * radius;
						NewPos.y = pivot.transform.position.y + height;
			transform.position = NewPos;
						transform.forward = -dist.normalized;
						Debug.DrawRay (transform.position, transform.forward * radius, Color.green);
				}			
		}
}
