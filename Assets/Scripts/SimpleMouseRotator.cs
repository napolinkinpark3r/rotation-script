﻿using UnityEngine;

/// <summary>
/// Based on SimpleMouseRotator from Sample Assets by Unity Corporation
/// </summary>
public class SimpleMouseRotator2 : MonoBehaviour
{
		// A mouselook behaviour with constraints which operate relative to
		// this gameobject's initial rotation. Also can rotate separate object by vertical axis.
	
		// Only rotates around local X and Y.
	
		// Works in local coordinates, so if this object is parented
		// to another moving gameobject, its local constraints will
		// operate correctly
		// (Think: looking out the side window of a car, or a gun turret
		// on a moving spaceship with a limited angular range)
	
		// to have no constraints on an axis, set the rotationRange to 360 or greater.

		public Vector2 rotationRange = new Vector3 (70, 70);
		public float rotationSpeed = 10;
		public float dampingTime = 0.2f;
		public Transform verticalRotationObject;
		Vector3 targetAngles;
		Vector3 followAngles;
		Vector3 followVelocity;
		Quaternion originalRotation;
		Quaternion originalRotationVerticalObject;
	
		// Use this for initialization
		void Start ()
		{
				originalRotation = transform.localRotation;
				if (originalRotationVerticalObject != null) {
						originalRotationVerticalObject = transform.localRotation;
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
				// we make initial calculations from the original local rotation
				transform.localRotation = originalRotation;

				// read input from mouse or mobile controls
				float inputH = 0;
				float inputV = 0;

				inputH = Input.GetAxis ("Mouse X");
				inputV = Input.GetAxis ("Mouse Y");
				// wrap values to avoid springing quickly the wrong way from positive to negative
				if (targetAngles.y > 180) {
						targetAngles.y -= 360;
						followAngles.y -= 360;
				}
				if (targetAngles.x > 180) {
						targetAngles.x -= 360;
						followAngles.x -= 360;
				}
				if (targetAngles.y < -180) {
						targetAngles.y += 360;
						followAngles.y += 360;
				}
				if (targetAngles.x < -180) {
						targetAngles.x += 360;
						followAngles.x += 360;
				}

				// with mouse input, we have direct control with no springback required.
				targetAngles.y += inputH * rotationSpeed;
				targetAngles.x += inputV * rotationSpeed;

				// clamp values to allowed range
				targetAngles.y = Mathf.Clamp (targetAngles.y, -rotationRange.y * 0.5f, rotationRange.y * 0.5f);
				targetAngles.x = Mathf.Clamp (targetAngles.x, -rotationRange.x * 0.5f, rotationRange.x * 0.5f);

				// smoothly interpolate current values to target angles
				followAngles = Vector3.SmoothDamp (followAngles, targetAngles, ref followVelocity, dampingTime);

				// update the actual gameobject's rotation
				if (verticalRotationObject == null) {
						transform.localRotation = originalRotation * Quaternion.Euler (-followAngles.x, followAngles.y, 0f);
				} else {
						transform.localRotation = originalRotation * Quaternion.Euler (0f, followAngles.y, 0f);
						verticalRotationObject.localRotation = originalRotationVerticalObject * Quaternion.Euler (-followAngles.x, 0f, 0f);
				}
		}


}
